#include <stdio.h>
#define MAXSIZE 100
typedef struct
{
    int i, j;
    int v;
} Triple;
typedef struct
{
    Triple data[MAXSIZE + 1];
    int mu, nu, tu; //总行号，列号，非0元素个数
} TSMatrix;

//创建稀疏矩阵

void create(TSMatrix *x)
{
    int i, j, v;
    printf("请依次输入该矩阵的总行号,列号,非0元素个数\n");
    printf("总行号:");
    scanf("%d", &x->mu);
    printf("\n总列号:");
    scanf("%d", &x->nu);
    printf("\n非0元素个数:");
    scanf("%d", &x->tu);
    printf("输入各行列及数据值:\n");
    for (int a = 1; a <= x->tu; a++)
    {
        scanf("%d%d%d", &i, &j, &v);
        x->data[a].i = i;
        x->data[a].j = j;
        x->data[a].v = v;
    }
}

//显示矩阵

void show(TSMatrix *x)
{
    int re[x->mu][x->nu];
    for (int a = 0; a < x->mu; a++)
    {
        for (int b = 0; b < x->nu; b++)
        {
            re[a][b] = 0;
        }
    }
    for (int a = 1; a <= x->tu; a++)
    {
        re[x->data[a].i - 1][x->data[a].j - 1] = x->data[a].v;
    }

    for (int a = 0; a < x->mu; a++)
    {
        for (int b = 0; b < x->nu; b++)
        {
            printf("%d  ", re[a][b]);
        }
        printf("\n");
    }
}
//比较两数大小comp(c1, c2)
int comp(int c1, int c2)
{
    if (c1 < c2)
    {
        return -1;
    }
    else if (c1 == c2)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}
//矩阵相加
int add(TSMatrix M, TSMatrix N, TSMatrix *Q)
{
    int m = 1, n = 1, q = 0;
    Q->mu = M.mu;
    Q->nu = M.nu;
    while (m <= M.tu && n <= N.tu)
    {
        switch (comp(M.data[m].i, N.data[n].i))
        {
        case -1:
            Q->data[++q] = M.data[m++];
            break;
        case 0:
            switch (comp(M.data[m].j, N.data[n].j))
            {
            case -1:
                Q->data[++q] = M.data[m++];
                break;
            case 0:
                Q->data[++q] = M.data[m++];
                Q->data[q].v += N.data[n++].v; //行列都相等时相加
                if (Q->data[q].v == 0)
                {
                    q--;
                }
                break;
            case 1:
                Q->data[++q] = N.data[n++];
                break;
            }
            break;
        case 1:
            Q->data[++q] = N.data[n++];
            break;
        }
    }
    while (m <= M.tu) // M矩阵剩余的三元数组加入Q中
    {
        Q->data[++q] = M.data[m++];
    }
    while (n <= N.tu) // N矩阵剩余的三元数组加入Q中
    {
        Q->data[++q] = N.data[n++];
    }
    if (q > MAXSIZE) //超过矩阵最大非零个数
    {
        printf("非0元素超出MAXSIZE");
        return 0;
    }
    Q->tu = q;
    return 1;
}
//快速转置
void Transpopse(TSMatrix M, TSMatrix *T)
{
    T->mu = M.mu;
    T->tu = M.tu;
    T->nu = M.nu;
    int num[M.tu + 1], cpot[M.tu + 1];
    int col, q;
    if (M.tu)
    {
        for (col = 1; col <= M.nu; col++)
        {
            num[col] = 0;
        }
        for (int t = 1; t <= M.tu; ++t)
        { //统计第col列中非零元的个数
            ++num[M.data[t].j];
        }
        cpot[1] = 1;
        for (col = 2; col <= M.nu; ++col)
        {
            cpot[col] = cpot[col - 1] + num[col - 1];
        }
        for (int p = 1; p <= M.tu; ++p)
        {
            col = M.data[p].j;
            q = cpot[col];
            T->data[q].i = M.data[p].j;
            T->data[q].j = M.data[p].i;
            T->data[q].v = M.data[p].v;
            ++cpot[col];
        }
    }
}
//矩阵相乘
int Mult(TSMatrix a, TSMatrix b, TSMatrix *y)
{
    int i, j, tp, t, p, q, brow, ctemp[a.mu + 1], a_num[a.mu + 1], b_num[b.mu + 1], a_rops[a.mu + 1], b_rops[b.mu + 1]; //每行第一个非零元素在data数组中的位置
    y->mu = a.mu;
    y->nu = b.nu;
    y->tu = 0;
    for (int z = 1; z <= a.nu; z++)
    {
        a_num[z] = 0;
        b_num[z] = 0;
    }
    for (int z = 1; z <= a.tu; ++z)
    { //统计第t行中非零元的个数
        ++a_num[a.data[z].i];
    }
    a_rops[1] = 1;
    for (int col = 2; col <= a.mu; ++col)
    {
        a_rops[col] = a_rops[col - 1] + a_num[col - 1];
    }

    for (int z = 1; z <= b.tu; ++z)
    { //统计第t行中非零元的个数
        ++b_num[b.data[z].i];
    }
    b_rops[1] = 1;
    for (int col = 2; col <= b.mu; ++col)
    {
        b_rops[col] = b_rops[col - 1] + b_num[col - 1];
    }

    if (a.tu * b.tu != 0) // y是非零矩阵
    {
        for (i = 1; i <= a.mu; i++) //从a的第一行开始，到最后一行
        {
            for (j = 1; j <= b.nu; j++) //从b的第一列到最后一列
            {
                ctemp[j] = 0; // 当前行的各列元素累加器清零
            }
            if (i < a.mu)
            {
                tp = a_rops[i + 1];
            }
            else
            {
                tp = a.tu + 1;
            }
            for (p = a_rops[i]; p < tp; p++)
            {
                brow = a.data[p].j;
                if (brow < b.mu)
                {
                    t = b_rops[brow + 1];
                }
                else
                {
                    t = b.tu + 1;
                }
                for (q = b_rops[brow]; q < t; q++)
                {
                    j = b.data[q].j;
                    ctemp[j] += a.data[p].v * b.data[q].v;
                }
            }
            for (j = 1; j <= y->nu; j++)
            {
                if (ctemp[j])
                {
                    if (++y->tu > MAXSIZE)
                    {
                        printf("非0元素超出MAXSIZE");
                        return 0;
                    }
                    y->data[y->tu].i = i;
                    y->data[y->tu].j = j;
                    y->data[y->tu].v = ctemp[j];
                }
            }
        }
    }
    return 1;
}
int main(void)
{
    TSMatrix A, B, C, X, Y;
    int a, flag = 1;
    printf("创建稀疏矩阵A\n");
    create(&A);
    printf("稀疏矩阵A:\n");
    show(&A);
    printf("创建稀疏矩阵B\n");
    create(&B);
    printf("稀疏矩阵B:\n");
    show(&B);
    while (flag)
    {
        printf("-----------------操  作-----------------\n-----------------1.相加-----------------\n---------------2.A的转置矩阵------------\n----------------3.矩阵乘法--------------\n-----------------4.退出-----------------\n");
        printf("输入操作数:(1/2/3/4):");
        scanf("%d", &a);
        if (a == 1)
        {
            int x = add(A, B, &C);
            if (x == 1)
            {
                printf("相加后的矩阵C:\n");
                show(&C);
            }
        }
        else if (a == 2)
        {
            Transpopse(A, &X);
            printf("将A转置后的矩阵D:\n");
            show(&X);
        }
        else if (a == 3)
        {
            int obj = Mult(A, B, &Y);
            if (obj == 1)
            {
                printf("矩阵A和B的乘积:\n");
                show(&Y);
            }
        }
        else if (a == 4)
        {
            flag = 0;
        }
        else
        {
            printf("输入的操作数错误,重新输入\n");
        }
    }
    printf("操作结束!");
}