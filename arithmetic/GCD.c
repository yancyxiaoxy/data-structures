#include<stdio.h>
unsigned int gcd(unsigned int m,unsigned int n)
{
    unsigned int rem; /*定义一个无符号整型变量*/
    while(n > 0)                 /*辗转相除法*/
    {
        rem = m % n;           /*取余操作*/
        m = n;
        n = rem;
    }
    return m;
}
int main(void)
{
    int a,b;
    printf("请输入任意两个正整数：\n");
    scanf("%d %d",&a,&b);
    printf("%d和%d的最大公约数是： ",a,b);
    printf("%d\n",gcd(a,b));        /*输出结果值*/
    return 0;
}