#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

#define MAX 50000

void InsSort(int r[], int length)
{
    int i, j;
    for (i = 2; i <= length; i++)
    {
        r[0] = r[i];
        j = i - 1;
        while (r[0] < r[j])
        {
            r[j + 1] = r[j];
            j = j - 1;
        }
        r[j + 1] = r[0];
    }
} //插入排序

void swapxy(int *x, int *y)
{
    int z;
    z = *x;
    *x = *y;
    *y = z;
}
int median3(int a[], int left, int right)
{
    int center = (left + right) / 2;
    if (a[left] > a[center])
        swapxy(&a[left], &a[center]);
    if (a[left] > a[right])
        swapxy(&a[left], &a[right]);
    if (a[center] > a[right])
        swapxy(&a[center], &a[right]);
    swapxy(&a[center], &a[right - 1]);
    return a[right - 1];
}
void insertionsort(int a[], int n)
{
    int j, p;
    int tmp;
    for (p = 1; p <= n; p++)
    {
        tmp = a[p];
        for (j = p; j > 0 && a[j - 1] > tmp; j--)
            a[j] = a[j - 1];
        a[j] = tmp;
    }
}
void qsortt(int a[], int left, int right)
{
    int i, j;
    int pivot;
    if (left + 2 <= right)
    {
        pivot = median3(a, left, right);
        i = left;
        j = right - 1;
        for (;;)
        {
            while (a[++i] < pivot)
            {
            }
            while (a[--j] > pivot)
            {
            }
            if (i < j)
                swapxy(&a[i], &a[j]);
            else
                break;
        }
        swapxy(&a[i], &a[right - 1]);
        qsortt(a, left, i - 1);
        qsortt(a, i + 1, right);
    }
    else
        insertionsort(a + left, right - left + 1);
}
void quicksort(int a[], int n)
{
    qsortt(a, 0, n - 1);
} //快速排序

void BubbleSort(int r[], int length)
{
    int i, j, temp;
    for (j = length; j > 0; j--)
        for (i = 0; i < j - 1; i++)
            if (r[i] > r[i + 1])
            {
                temp = r[i];
                r[i] = r[i + 1];
                r[i + 1] = temp;
            }
} //冒泡排序

void ShellInsert(int r[], int length, int delta)
{
    int i, j;
    for (i = 1 + delta; i <= length; i++)
        if (r[i] < r[i - delta])
        {
            r[0] = r[i];
            for (j = i - delta; j > 0 && r[0] < r[j]; j -= delta)
                r[j + delta] = r[j];
            r[j + delta] = r[0];
        }
}
void ShellSort(int r[], int length, int delt[], int n)
{
    int i;
    for (i = 0; i <= n - 1; ++i)
        ShellInsert(r, length, delt[i]);
} //希尔排序

int main()
{
    long dwStart, dwStop, runtime;
    int num[MAX], num1[MAX], a[MAX], i, j;
    FILE *fp = NULL;
    fp = fopen("test.txt", "w+");
    // srand(time(0));
    for (i = 0; i < MAX; i++)
        num[i] = rand();
    fprintf(fp, "%d ", num[i]);
    printf("生成%d个随机数\n", MAX);

    printf("********平均排序时间********\n");
    for (i = 0; i < MAX; i++)
        a[i] = num[i];
    dwStart = clock();
    InsSort(a, MAX);
    dwStop = clock();
    runtime = dwStop - dwStart;
    printf("使用插入排序运行了%ldms\n", runtime);

    for (i = 0; i < MAX; i++)
        a[i] = num[i];
    dwStart = clock();
    quicksort(a, MAX);
    dwStop = clock();
    runtime = dwStop - dwStart;
    printf("使用快速排序运行了%ldms\n", runtime);

    for (i = 0; i < MAX; i++)
        a[i] = num[i];
    dwStart = clock();
    BubbleSort(a, MAX);
    dwStop = clock();
    runtime = dwStop - dwStart;
    printf("使用冒泡排序运行了%ldms\n", runtime);

    int delt[10] = {100, 80, 60, 40, 20, 10, 5, 3, 2, 1};
    for (i = 0; i < MAX; i++)
        a[i] = num[i];
    dwStart = clock();
    ShellSort(a, MAX, delt, 10);
    dwStop = clock();
    runtime = dwStop - dwStart;
    printf("使用希尔排序运行了%ldms\n", runtime);
    printf("\n排序测试完成！\n");
    getchar();
    printf("输入任意字符，按下回车继续！");
    //有序数
    // for(i=1i<=MAXi++)
    for (i = MAX; i >= 1; i--)
        num1[i] = i;
    printf("\n生成%d个有序数\n", MAX);

    printf("********平均排序时间********\n");
    for (i = 0; i < MAX; i++)
        a[i] = num1[i];
    dwStart = clock();
    InsSort(a, MAX);
    dwStop = clock();
    runtime = dwStop - dwStart;
    printf("使用插入排序运行了%ldms\n", runtime);

    for (i = 0; i < MAX; i++)
        a[i] = num1[i];
    dwStart = clock();
    quicksort(a, MAX);
    dwStop = clock();
    runtime = dwStop - dwStart;
    printf("使用快速排序运行了%ldms\n", runtime);

    for (i = 0; i < MAX; i++)
        a[i] = num1[i];
    dwStart = clock();
    BubbleSort(a, MAX);
    dwStop = clock();
    runtime = dwStop - dwStart;
    printf("使用冒泡排序运行了%ldms\n", runtime);

    int delt1[10] = {100, 80, 60, 40, 20, 10, 5, 3, 2, 1};
    for (i = 0; i < MAX; i++)
        a[i] = num1[i];
    dwStart = clock();
    ShellSort(a, MAX, delt1, 10);
    dwStop = clock();
    runtime = dwStop - dwStart;
    printf("使用希尔排序运行了%ldms\n", runtime);
    printf("\n排序测试完成！\n");
    getchar();
    return 0;
}