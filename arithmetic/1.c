#include <stdio.h>
#include <malloc.h>

int IsSwap(int *str, int nBegin, int nEnd) //保证每次填入的数一定是这个数所在重复数集合中「从左往右第一个未被填过的数字」
{
    for (int i = nBegin; i < nEnd; i++)
        if (str[i] == str[nEnd])
            return 0;
    return 1;
}
// k表示当前选取到第几个数，m表示数组大小
void AllRange(int *str, int k, int m)
{
    if (k == m)
    {
        for (int i = 0; i < m; i++)
            printf("%d",str[i]);
        printf("\n");
    }
    else
    {
        for (int i = k; i < m; i++)
        {
            if (IsSwap(str, k, i)) //同一层中相同的元素只有第一个有用
            {
                int temp1 = str[k];  
                str[k] = str[i];  
                str[i] = temp1;  
                AllRange(str, k + 1, m);
                int temp2 = str[k];  
                str[k] = str[i];  
                str[i] = temp2;  
            }
        }
    }
}
int main()
{
    // cout<<"数组中没有重复元素的全排列:"<<endl;
    // int n;
    // cout<<"请输入数组大小:";
    // while(cin>>n && n!=0)
    // {
    //     cout<<"请输入数组元素:"<<endl;
    //     int *a = new int[n];
    //     for(int i=0; i<n; i++) cin>>a[i];
    //     Perm(a, 0, n);
    //     cout<<"请输入数组大小:";
    // }
    printf("数组中有重复元素的全排列:\n");
    int n;
    printf("请输入数组大小:");
    scanf("%d", &n);
    while (n != 0)
    {
        printf("输入数组元素:\n");
        int *a = (int*)malloc(sizeof(int) * n);
        for (int i = 0; i < n; i++)
            scanf("%d", a+i);
        printf("全排列为:\n");
        AllRange(a, 0, n);
    }
    return 0;
}
