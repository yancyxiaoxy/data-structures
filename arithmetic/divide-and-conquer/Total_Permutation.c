#include <stdio.h>

int n = 0;

void swap(int *a, int *b)
{
    int m;
    m = *a;
    *a = *b;
    *b = m;
}
void perm(int list[], int k, int m)
{
    int i;
    if (k == m)
    {
        for (i = 0; i <= m; i++)
            printf("%d ", list[i]);
        printf("\n");
        n++;
    }
    else
    {
        for (i = k; i <= m; i++)
        {
            swap(&list[k], &list[i]);
            perm(list, k + 1, m);
            swap(&list[k], &list[i]);
        }
    }
}
int main()
{
    int list[] = {1, 2, 3, 4, 5};
    perm(list, 0, 4);
    printf("total:%d\n", n);
    return 0;
}
// 我们最开始有一个list数组，我们首先从第一位开始排，第一位的值有五种可能，我们先设置一个变量k，它表示我们目前排到了第几位，然后分别让
// list[k]和后面几位的元素交换位置，每交换好一次，就确定了那一位的值，然后开始往下一位继续递归，递归完毕后需要恢复到原来的位置，
// 再让list[k]和下一位交换位置。
// 总结来说就是三个步骤：
// ① 交换元素位置
// ② 递归
// ③ 恢复元素位置
// ————————————————
// 版权声明：本文为CSDN博主「boomchenchen」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
// 原文链接：https://blog.csdn.net/weixin_41710054/article/details/106885706