#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAXSIZE 16
int Board[MAXSIZE][MAXSIZE];
int title=1;//全局序号

//tr：棋盘左上角方格的行号
//tc：棋盘左上角方格的列号
//dr：特殊方格所在的行号
//dc：特殊方格所在的列号
//size：size=2^k 棋盘规格为2^k*2^k
void ChessBoard(int tr,int tc,int dr,int dc,int size)
{
    if(size==1)
        return;
    int s = size/2;//分割棋盘
    int t = title++;
    //覆盖左上角子棋盘
    if(dr <tr+s && dc<tc+s) //特殊方格在此棋盘中
        ChessBoard(tr,tc,dr,dc,s);
    else//特殊方格不在棋盘中
    {
        Board[tr+s-1][tc+s-1]=t;//用t号L型骨牌覆盖右下角
        ChessBoard(tr,tc,tr+s-1,tc+s-1,s);//覆盖其余方格
    }
    //覆盖右上角子棋盘
    if(dr<tr+s && dc>=tc+s)//特殊方格在此棋盘中
    {
        ChessBoard(tr,tc+s,dr,dc,s);
    }
    else
    {
        Board[tr+s-1][tc+s]=t;//用t号L型骨牌覆盖左下角
        ChessBoard(tr,tc+s,tr+s-1,tc+s,s);//覆盖其余方格
    }
    //覆盖左下角子棋盘
    if(dr>=tr+s && dc<tc+s)//特殊方格在此棋盘中
    {
        ChessBoard(tr+s,tc,dr,dc,s);
    }
    else
    {
        Board[tr+s][tc+s-1]=t;//用t号L型骨牌覆盖右上角
        ChessBoard(tr+s,tc,tr+s,tc+s-1,s);//覆盖其余方格
    }
    //覆盖右下角子棋盘
    if(dr>=tr+s && dc>=tc+s)//特殊方格在此棋盘中
    {
        ChessBoard(tr+s,tc+s,dr,dc,s);
    }
    else
    {
        Board[tr+s][tc+s] = t;//用t号L型骨牌覆盖左上角
        ChessBoard(tr+s,tc+s,tr+s,tc+s,s);//覆盖其余方格
    }

}
int main()
{
    int i ,j;
    Board[0][1]=0;
    ChessBoard(0,0,2,3,MAXSIZE);
    for (i = 0;i<MAXSIZE;i++)
    {
        for (j=0;j<MAXSIZE;j++)
            printf("%5d",Board[i][j]);
        printf("\n");
    }
    return 0;
}