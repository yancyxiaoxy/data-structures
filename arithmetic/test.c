#include <stdio.h>
void sort(int *p, int n)
{
    int i, j, k;
    for (j = 0; j < n - 1; j++)
        for (i = 0; i < n - j - 1; i++)
            if (*(p + i) > *(p + i + 1))
            {
                k = *(p + i);
                *(p + i) = *(p + i + 1);
                *(p + i + 1) = k;
            }
}

void main()
{
    int a[3][3];
    int i, j, m = 9;
    for (i = 0; i < 3; i++)
        for (j = 0; j <3; j++)
            scanf("%d", &a[i][j]);

    sort(a[0], m);

    for (i = 0; i < 3; i++)
    {
        printf("\n");
        for (j = 0; j < 3; j++)
            printf("%5d ", a[i][j]);
    }
}