#include <stdio.h>
int min(int i, int j)
{
    if (i < j)
        return i;
    else
        return j;
}
int main()
{
    int n, m, i, j;
    printf("输入总共有多少种硬币 m：\n");
    scanf("%d", &n);
    printf("输入这m种硬币的面值：\n");

    int a[n];
    for (i = 0; i < n; i++)
    {
        scanf("%d", &a[i]);
    }
    printf("输入需要凑足的n元：\n");
    scanf("%d", &m);
    int dp[m + 1];
    dp[0] = 0;
    for (i = 1; i <= m; i++)
    {
        dp[i] = m + 1;
    }
    for (i = 1; i <= m; i++)
    {
        for (j = 0; j < n; j++)
        {
            if (a[j] <= i)
            {
                dp[i] = min(dp[i], dp[i - a[j]] + 1);
            }
        }
    }
    if (dp[m] != m + 1)
    {
        printf("最少的硬币数：\n");
        printf("%d", dp[m]);
    }
    else
    {
        printf("Impossible");
    }
}
