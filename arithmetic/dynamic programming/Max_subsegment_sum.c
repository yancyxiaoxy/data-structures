#include <stdio.h>
int MaxSum_DYN(int *v, int n)
{
    int sum = 0, b = 0;
    int i;
    for (i = 0; i < n; i++)
    {
        if (b > 0)
            b += v[i];
        else
            b = v[i];
        if (b > sum)
            sum = b;
    }
    return sum;
}
void main()
{
    int n[5];
    printf("请依次输入5个数字：\n");
    for (int i = 0; i < 5; i++)
    {
        scanf("%d", &n[i]);
    }
    int sum = MaxSum_DYN(n, 5);
    printf("最大子段和为：%d\n", sum);
}