#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#define MAX 5000

int k, len;

//将数组数组首元素a[p]作为基准数将数组分割
int Partition(int a[], int p, int r);
//交换两个元素
void swap1(int *a, int *b);
// //在数组中随机选择一个数将数组分割
int RandomizedPartition(int a[], int p, int r);
// //产生随机数
int Randself(int x, int y);
//线性划分
int RandomizedSelect(int a[], int p, int r, int k);

// p为首元素下标，r为尾元素下标
int Partition(int a[], int p, int r)
{
    // i指向首元素，j指向尾元素的下一个元素
    int i = p, j = r + 1;
    //将首元素作为基准数
    int x = a[p];
    while (1)
    {
        // i从基准数左边的元素开始找，直到找到第一个大于等于基准数的元素
        while (a[++i] < x && i < r)
            ;
        // j从尾元素开始找，直到找到第一个小于等于基准数的元素
        while (a[--j] > x)
            ;
        //若i>=j，说明基准数的位置已找到，为j
        if (i >= j)
        {
            break;
        }
        //交换两个元素，使得基准数左边的数均不大于它，右边的数均不小于它
        swap1(&a[i], &a[j]);
    }
    //将基准数归位
    a[p] = a[j];
    a[j] = x;
    //返回基准数的位置
    return j;
} //快速排序

void swap1(int *a, int *b)
{
    int temp;
    temp = *a;
    *a = *b;
    *b = temp;
}

int RandomizedPartition(int a[], int p, int r)
{
    //在p和r之间找一个随机数
    int i = Randself(p, r);
    swap1(&a[i], &a[p]);
    return Partition(a, p, r);
}

int Randself(int x, int y)
{
    return x + rand() % (y - x);
}

int RandomizedSelect(int a[], int p, int r, int k)
{
    //数组被分割成只剩下一个元素，该元素就是第k小的元素
    if (p == r)
    {
        return a[p];
    }
    //在数组中随机找一个数将数组分割，分成小于等于该基准的数组和大于该基准的数组
    int i = RandomizedPartition(a, p, r);
    //求较小数数组的长度
    len = i - p + 1;
    //若较小数数组的长度小于等于k，说明第k小的元素在这个数组内，将其递归
    if (k <= len)
    {
        return RandomizedSelect(a, p, i, k);
    }
    //否则，说明第k小的元素在较大数数组，将其递归
    else
    {
        return RandomizedSelect(a, i + 1, r, k - len);
    }
}
int main()
{
    int k;
    int num[MAX];
    FILE *fp = NULL;
    fp = fopen("test.txt", "w+");
    //srand(time(0));
    for ( int i = 0; i < MAX; i++)
    {
        num[i] = rand();
        fprintf(fp, "%d ", num[i]);
    }
    fclose(fp);
    //输入第k小
    printf("\n输入第k小的元素k：");
    scanf("%d", &k);
    //找第k小并返回
    int res = RandomizedSelect(num, 0, MAX - 1, k);
    printf("%d", res);
    return 0;
}