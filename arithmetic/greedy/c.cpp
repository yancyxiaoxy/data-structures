#include <iostream>
using namespace std;
//该算法没有将结束时间非减排序
int s[6] = {0, 1, 2, 4, 6, 8}, f[6] = {0, 11, 3, 5, 7, 9};
bool a[5];
int n = 5;
int Selector()
{
	a[1] = true;
	int j = 1;
	int count = 1;
	for (int i = 2; i <= n; i++)
	{
		if (s[i] >= f[j])
		{
			a[i] = true;
			j = i;
			count++;
		}
		else
			a[i] = false;
	}
	return count;
}

int main()
{
	cout << "活动序号：" << endl;
	for (int i = 1; i <= 5; i++)
		cout << i << " ";
	cout << endl
		 << "活动开始时间：" << endl;
	for (int i = 1; i <= 5; i++)
		cout << s[i] << " ";
	cout << endl
		 << "活动结束时间：" << endl;
	for (int i = 1; i <= 5; i++)
		cout << f[i] << " ";
	int count = Selector();
	cout << endl
		 << "一共选择个" << count << "活动如下：" << endl;
	for (int i = 0; i <= n; i++)
		if (a[i])
			cout << i << " ";
	return 0;
}