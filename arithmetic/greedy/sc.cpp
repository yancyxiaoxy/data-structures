#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <string>

using namespace std;

int n, ans = 1, t;
struct app
{
    int s, e;
} a[1005];

bool cmp(app x, app y)
{
    return x.e < y.e;
}

int main()
{
    printf("请输入活动的数量：\n");
    scanf("%d", &n);
    printf("请输入活动开始时间：\n");
    for (int i = 1; i <= n; i++)
        scanf("%d", &a[i].s);
    printf("请输入活动结束时间：\n");
    for (int i = 1; i <= n; i++)
        scanf("%d", &a[i].e);

    sort(a + 1, a + n + 1, cmp);

    t = a[1].e;

    for (int i = 2; i <= n; i++)
    {
        if (a[i].s >= t)
        {
            ans++;
            t = a[i].e;
        }
    }

    printf("可安排的活动数为：\n");
    printf("%d", ans);

    return 0;
}
