#include <stdio.h>
#include <stdlib.h>
#define MAX 5000

int n, k, len;

//选择排序
void SelectSort(int a[], int p, int r);
//将x作为基准数将数组分割，返回x的位置
int Partition(int a[], int p, int r, int x);
//交换两个元素
void swap1(int *a, int *b);
//找每组的中位数,返回中位数的位置i
int SearchMid(int a[], int p, int r);
//线性划分
int Select(int a[], int p, int r, int k);

int main()
{
      int k;
    int num[MAX];
        FILE *fp = NULL;
     fp = fopen("test.txt", "w+");
    for ( int i = 0; i < MAX; i++)
    {
        num[i] = rand();
        fprintf(fp, "%d ", num[i]);
    }
    fclose(fp);
    //输入第k小
    printf("输入第k小的元素k：");
    scanf("%d", &k);
    //找第k小并返回
    int res = Select(num, 0, MAX - 1, k);
    printf("%d", res);
    return 0;
}

void SelectSort(int a[], int p, int r)
{
    for (int i = p; i < r; ++i)
    {
        int index = i;
        for (int j = i + 1; j <= r; ++j)
        {
            if (a[j] < a[index])
            {
                index = j;
            }
        }
        swap1(&a[i], &a[index]);
    }
}

int Partition(int a[], int p, int r, int x)
{
    //i指向首元素的前一个位置，j指向尾元素的后一个位置
    int i = p - 1, j = r + 1;
    while (1)
    {
        //i从基准数右边的元素开始找，直到找到第一个大于等于基准数的元素
        while (a[++i] < x && i < r);
        //j从尾元素开始找，直到找到第一个小于等于基准数的元素
        while (a[--j] > x && j > p);
        //若i>=j，说明基准数的位置已找到，为j
        if (i >= j)
        {
            break;
        }
        //交换两个元素，使得基准数左边的数均不大于它，右边的数均不小于它
        swap1(&a[i], &a[j]);
    }
    //返回基准数的位置
    return j;
}

void swap1(int *a, int *b)
{
    int temp;
    temp = *a;
    *a = *b;
    *b = temp;
}

int SearchMid(int a[], int p, int r)
{
    //建立与数组a同等大小的数组b
    int b[r - p + 1];
    //用数组b存放数组a（注意此时b的首地址为0，而a的首地址为p）
    for (int i = p; i <= r; ++i)
    {
        b[i - p] = a[i];
    }
    //将数组b排序，b[(r-p+1)/2]为中位数
    SelectSort(b, 0, r - p);
    for (int i = p; i <= r; ++i)
    {
        if (a[i] == b[(r - p + 1) / 2])
        {
            return i;
        }
    }
    return 0;
}

int Select(int a[], int p, int r, int k)
{
    if (r - p < 5)
    {
        SelectSort(a, p, r);
        return a[p + k - 1];
    }
    //分成n/5组，每组5个，找到每组的中位数并将它放到数组首元素的位置
    for (int i = 0; i <= (r - p - 4) / 5; ++i)
    {
        int mid = SearchMid(a, p + 5 * i, p + 5 * i + 4);
        swap1(&a[mid], &a[p + i]);
    }
    //找到各组中位数的中位数
    int x = Select(a, p, p + (r - p - 4) / 5, (r - p - 4) / 10 + 1);
    //按照中位数划分
    int i = Partition(a, p, r, x);
    //求较小数数组的长度
    len = i - p + 1;
    //若较小数数组的长度小于等于k，说明第k小的元素在这个数组内，将其递归
    if (k <= len)
    {
        return Select(a, p, i, k);
    }
    //否则，说明第k小的元素在较大数数组，将其递归
    else
    {
        return Select(a, i + 1, r, k - len);
    }
}