#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

typedef int SElemType;
#define MAXSIZE 100
typedef struct
{
    SElemType *base;
    SElemType *top;
    int stacksize; //栈可用最大容量
} SqStack;

//初始化栈
int StackEmpty(SqStack *S)
{
    S->base = (SElemType *)malloc(MAXSIZE * sizeof(SElemType));
    if (!S->base)
        return 0;
    S->top = S->base;
    S->stacksize = MAXSIZE;
    return 1;
}
//入栈
void push(SqStack *S, SElemType x)
{
    if ((S->top) - (S->base) == S->stacksize) //栈满
    {
        S->base = (SElemType *)realloc(S->base, (S->stacksize + 1) * sizeof(SElemType));
        if (!S->base)
        {
            exit(-1);
        }
        S->stacksize++;
    }
    *S->top = x;
    S->top++;
}
//出栈
SElemType pop(SqStack *S)
{
    SElemType y;
    if ((S->top) == (S->base)) //判断队为空
    {
        return 0;
    }
    --S->top;
    y = *S->top;
    return y;
}

int main(void)
{
    SqStack S;
    int i, j, x = 0;
    SElemType y;
    if (!StackEmpty(&S))
    {
        printf("初始化失败");
    }
    else
    {
        printf("请输入一个十进制整数:");
        scanf("%d", &i);
        printf("请输入要转化的进制:");
        scanf("%d", &j);
        while (i != 0)
        {
            push(&S, (i % j));
            i = i / j;
            x++;
        }
        int *a = (int *)malloc(sizeof(int) * x);
        for (int z = 0; z < x; z++)
        {
            a[z] = pop(&S);
        }

        printf("转化后的结果为:\n");

        if (j == 16)
        {
            for (int i = 0; i < x; i++)
            {
                switch (a[i])
                {
                case 10:
                    printf("A");
                    continue;
                case 11:
                    printf("B");
                    continue;
                case 12:
                    printf("C");
                    continue;
                case 13:
                    printf("D");
                    continue;
                case 14:
                    printf("E");
                    continue;
                case 15:
                    printf("F");
                    continue;
                default:
                    printf("%d", a[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < x; i++)
            {
                printf("%d", a[i]);
            }
        }
    }
}