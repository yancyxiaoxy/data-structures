#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
typedef int ElemType;

typedef struct node
{
    ElemType data;
    struct node *next;
} LNode, *Linklist;

//尾插法
Linklist creatsLinkW(int m)
{
    Linklist head, p, r;
    int i;
    if (m < 1)
    {
        return NULL;
    }
    r = head = (Linklist)malloc(sizeof(LNode)); //创建头节点,尾指针r指向头节点
    r->next = NULL;
    for (i = 0; i < m; i++)
    {
        p = (Linklist)malloc(sizeof(LNode));
        scanf("%d", &p->data);
        p->next = NULL;
        r->next = p; //插入到链尾
        r = p;
    }
    return head;
}

//输出链表
void print(Linklist L)
{
    Linklist p;
    ElemType j;
    p = L->next;
    if (p == NULL)
    {
        printf("链表为空");
        return;
    }
    printf("单链表的值为:\n");
    while (p)
    {
        j = p->data;
        printf("%d", j);
        p = p->next;
        printf("\n");
    }
}

//求链长
int length(Linklist head)
{
    int i = 0;
    Linklist r;
    r = head->next;
    while (r)
    {
        r = r->next;
        i++;
    }
    return i;
}

//删除第i个节点
int deletei(Linklist head, int n)
{
    Linklist p, r;
    int i = 0,j;
    p = head;

for(j=1;p->next!=NULL;j++){
	if(j==n){
		r=p->next;
		p->next=r->next;
		free(r);
        return 1;
	}
	else{
	p=p->next;
	}
}
 printf("没有这个节点");
        return 0;

    // while (p && i < n - 1)
    // {
    //     p = p->next;
    //     i++;
    // }
    // if (!(p->next) || i > n - 1)
    // {
    //     printf("没有这个节点");
    //     return 0;
    // }
    // r = p->next;
    // p->next = r->next;
    // free(r);
    // return 1;
}
//删除值相同节点
int deleteiT(Linklist head, int index)
{
    Linklist r, s;
    s = head;
    r = head->next;
    while (r && r->data != index)
    {
        s = r;
        r = r->next;
    }
    if (!r)
    {
        printf("没有该值的节点");
        return 0;
    }
    s->next = r->next;
    free(r);
    return 1;
}

//插入元素
int insert(Linklist head, int loc, int data)
{
    Linklist p, r;
    int j = 0;
    r = head;
    while (r && j < loc - 1)
    {
        r = r->next;
        j++;
    }
    if (!r || j > loc - 1)
    {
        printf("节点位置错误!");
        return 0;
    }

    p = (Linklist)malloc(sizeof(LNode));
    p->data = data;
    p->next = r->next;
    r->next = p;
    return 1;
}

int main(void)
{
    Linklist L;
    int i;
    int m;
    int t;
    int index;
    int loc;
    int data;
    printf("请输入要创建的节点数:\n");
    scanf("%d", &m);
    // 1.
    //构建链表
    printf("请输入%d个节点值:\n", m);
    L = creatsLinkW(m);
    //输出链表值
    print(L);
    // 2.
    printf("输入要删除第i个节点中,i的值\n");
    scanf("%d", &i);
    //删除第i个节点
    deletei(L, i) == 1 ? printf("删除成功\n") : printf("删除失败\n");
    //输出链表值
    print(L);
    // 3.
    printf("输入要删除的值\n");
    scanf("%d", &index);
    deleteiT(L, index) == 1 ? printf("删除成功\n") : printf("删除失败\n");
    //输出链表值
    print(L);
    // 4.
    printf("输入在第几个位置插入插入元素\n");
    scanf("%d", &loc);
    printf("输出要插入的值\n");
    scanf("%d", &data);
    insert(L, loc, data) == 1 ? printf("插入成功\n") : printf("插入失败\n");
    print(L);
}