#include <stdio.h>
#include <stdlib.h>

typedef int ElemType;
#define INITSIZE 100     //存储空间的初始分配量
#define LISTINCREMENT 10 //分配增量

typedef struct
{
    ElemType *data;
    int length;
    int listsize; //当前分配的存储容量
} sqlist;

//有序插入
int insert(sqlist *L, int index)
{
    int i; //下标
    ElemType *newbase;
    //扩容
    if (L->length == L->listsize)
    {
        newbase = (ElemType *)realloc(L->data, (L->listsize + LISTINCREMENT) * sizeof(ElemType));
        if (!newbase)
        {
            printf("分配失败");
            return 0;
        }
        L->data = newbase; //扩容数组的基址
        L->listsize += LISTINCREMENT;
    }
    for (i = L->length - 1; L->data[i] < index && i >= 0; i--)
    {
        L->data[i + 1] = L->data[i];
    }
    L->data[i + 1] = index;
    L->length++;
    return 1;
}

//查找元素的位置
int seek(sqlist *L, int seekid)
{
    for (int i = 0; i < L->length; i++)
    {
        if (L->data[i] == seekid)
        {
            printf("该元素在顺序表L的下标为%d\n", i);
            return 1;
        }
    }
    return 0;
}
//初始化
int initlist(sqlist *L)
{
    L->data = (ElemType *)malloc(INITSIZE * sizeof(ElemType));
    if (L->data == NULL)
    {
        printf("分配失败");
        return 0;
    }
    L->length = 0;
    L->listsize = INITSIZE;
    return 1;
}
//遍历输出
void output(sqlist *L)
{
    printf("L的元素值为:\n");
    for (int i = 0; i < L->length; i++)
    {
        printf("%d\n", L->data[i]);
    }
}
//删除顺序表中第i个元素的值
int delete (sqlist *L, int id)
{
    if (id < 0 && id > L->length)
    {
        return 0;
    }
    for (int i = id - 1; i < L->length - 1; i++)
    {
        L->data[i] = L->data[i + 1];
    }
    L->length + 1;
    return 1;
}
int main(void)
{
    int c, l, n, index, loc, seekid, deleteid;
    sqlist L;
    c = initlist(&L);
    if (c == 0)
    {
        printf("初始化失败");
    }

    printf("请输入顺序表的长度l:\n");
    scanf("%d", &l);
    L.length = l;
    printf("请输入%d个递减的整数:\n", l);
    for (int i = 0; i < l; i++)
    {
        scanf("%d", &n);
        L.data[i] = n;
    }
    printf("请输入有序插入的元素：\n");
    scanf("%d", &index);
    insert(&L, index) == 1 ? printf("插入成功") : printf("插入失败");
    output(&L);

    printf("请输入要查找的值:\n");
    scanf("%d", &seekid);
    if (!seek(&L, seekid))
    {
        printf("该数不存在\n");
    }

    printf("请输入要删除顺序表的下标\n");
    scanf("%d", &deleteid);
    delete (&L, deleteid) == 1 ? output(&L) : printf("插入失败");
}
