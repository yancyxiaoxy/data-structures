#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

typedef int ElemType;
#define INITSIZE 100     //存储空间的初始分配量
#define LISTINCREMENT 10 //分配增量

typedef struct
{
    ElemType *data;
    int length;
    int listsize; //当前分配的存储容量
} sqlist, *sqli;

//初始化
int initlist(sqlist *L)
{
    L->data = (ElemType *)malloc(INITSIZE * sizeof(ElemType)); //分配空间
    if (L->data == NULL)
    {
        printf("分配失败");
        return 0;
    }
    L->length = 0;
    L->listsize = INITSIZE;
    return 1;
}

//判断是否需要扩容
void bround(sqlist *L, int m)
{

    if (L->length == L->listsize || L->length > L->listsize)
    { //扩容
        ElemType *newbase;
        newbase = (ElemType *)realloc(L->data, (L->listsize + LISTINCREMENT) * sizeof(ElemType));
        if (!newbase)
        {
            printf("扩容分配失败");
            exit(0);
        }
        L->data = newbase; //扩容数组的基址
        L->listsize += LISTINCREMENT;
    }
}

//合并
int mergeSqlist(sqlist *A, sqlist *B, sqlist *C)
{
    int i = 0;
    int j = 0;
    int k = 0;
    C->listsize = C->length = A->length + B->length;
    C->data = (ElemType *)malloc(C->listsize * sizeof(ElemType));
    if (!C->data)
    {
        printf("C分配空间失败\n");
        return 0;
    }
    while (i < A->length && j < B->length)
    {
        if (A->data[i] >= B->data[j])
        {
            C->data[k++] = A->data[i++];
        }
        else
        {
            C->data[k++] = B->data[j++];
        }
    }
    while (i < A->length)
    {
        C->data[k++] = A->data[i++];
    }
    while (j < B->length)
    {
        C->data[k++] = B->data[j++];
    }
    return 1;
}

//新建顺序表
void create(sqlist *L, int m)
{
    int n;
    L->length = m;
    bround(L, m);
    printf("请输入%d个依次递减的整数:\n", m);
    for (int i = 0; i < m; i++)
    {
        scanf("%d", &n);
        L->data[i] = n;
    }
}

//输出元素
void list(sqlist *L)
{
    int j;
    for (j = 0; j < L->length; j++)
        printf("%d\t", L->data[j]);
    printf("\n");
}

int main(void)
{
    int a, b, m, n, i = 0, j = 0, x = 0;
    sqlist A, B, C;

    a = initlist(&A);
    if (a == 0)
    {
        printf("A内存分配失败\n");
    }
    else
    {
        printf("请输入顺序表A的长度m:\n");
        scanf("%d", &m);
        create(&A, m);
        printf("顺序表A中的元素为:\n");
        list(&A);
    }
    b = initlist(&B);
    if (b == 0)
    {
        printf("B内存分配失败\n");
    }
    else
    {
        printf("请输入顺序表B的长度n:\n");
        scanf("%d", &n);
        create(&B, n);
        printf("顺序表B中的元素为:\n");
        list(&B);
    }
    printf("合并A、B到C中\n");
    if (mergeSqlist(&A, &B, &C))
    {
        list(&C);
    }
    else
    {
        printf("合并失败!\n");
    }
}
