#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
typedef int ElemType;

typedef struct node
{
    ElemType data;
    struct node *next;
} LNode, *Linklist;

//尾插法
Linklist creatsLinkW(int m)
{
    Linklist head, p, r;
    int i;
    if (m < 1)
    {
        return NULL;
    }
    r = head = (Linklist)malloc(sizeof(LNode)); //创建头节点
    r->next = NULL;
    for (i = 0; i < m; i++)
    {
        p = (Linklist)malloc(sizeof(LNode));
        scanf("%d", &p->data);
        p->next = NULL;
        r->next = p; //插入到链尾
        r = p;
    }
    return head;
}
//输出链表值
void print(Linklist L)
{
    Linklist p;
    ElemType j;
    p = L->next;
    if (p == NULL)
    {
        printf("链表为空");
        return;
    }
    while (p)
    {
        j = p->data;
        printf("%d", j);
        p = p->next;
        printf("\n");
    }
}

//合并
void *mergeLnode(Linklist A, Linklist B, Linklist C)
{
    Linklist p, q, m;
    p = A->next;
    q = B->next;
    C = m = A; //将A的头结点作为C的头结点
    while (p && q)
    {
        if (p->data > q->data)
        {
            m->next = p;
            m = p;
            p = p->next;
        }
        else
        {
            m->next = q;
            m = q;
            q = q->next;
        }
    }

    m->next = p ? p : q;
    free(B);
    print(C);
}
int main(void)
{
    Linklist A, B, C;
    int i, j;
    printf("输入A链表的结点数:\n");
    scanf("%d", &i);
    printf("请按照递减顺序输入A链表创建的节点值:\n");
    A = creatsLinkW(i);

    printf("输入B链表的结点数:\n");
    scanf("%d", &j);
    printf("请按照递减顺序输入B链表创建的节点值:\n");
    B = creatsLinkW(j);
    printf("----------------\n");
    printf("A链表为:");
    print(A);
    printf("B链表为:");
    print(B);
    printf("合并后的C链表为:\n");
    mergeLnode(A, B, C);
}