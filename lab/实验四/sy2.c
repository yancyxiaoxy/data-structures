#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>

#define NumVertices 100
#define INFINITE 9999
int visited[NumVertices]; //访问标记数组是全局变量,0为false,1为true
int dfn[NumVertices];     //顶点的先深编号
typedef int EdgeData;

typedef struct node
{ //边表结点
    int adjvex;
    EdgeData cost;
    struct node *next;
} EdgeNode;

typedef struct
{ //顶点表结点
    int vertex;
    EdgeNode *firstedge;
} VertexNode;

typedef struct
{ //邻接表
    VertexNode vexlist[NumVertices];
    int n, e;
} AdjGraph;

typedef struct celltype
{
    int element;
    struct celltype *next;

} celltype;
typedef struct
{
    celltype *front;
    celltype *rear;
} Queue;

int Empty(Queue Q)
{
    if (Q.front == Q.rear)
        return 1;
    else
        return 0;
}

//入
void EnQueue(int x, Queue *Q)
{
    Q->rear->element = x;
    Q->rear->next = malloc(sizeof(celltype));
    Q->rear = Q->rear->next;
    Q->rear->next = NULL;
}

//出
int DeQueue(Queue *Q)
{
    int e;
    celltype *temp;
    if (Empty(*Q))
    {
        printf("空队列");
        return 0;
    }
    else
    {
        e = Q->front->element;
        temp = Q->front->next;
        Q->front->next = temp->next;
        free(temp);
        if (Q->front->next == NULL)
            Q->rear = Q->front;
    }
    return e;
}

//创建邻接矩阵
AdjGraph Create(AdjGraph M)
{
    // t:1-无向图； t:2-有向图;  t：3-带权无向图 ;t：4-带权有向图
    int t;
    int vdata;
    int i, j;
    int w; //权值
    printf("请输入顶点数、边数、图的类型，输入格式(均为数字)：v,e,t(t为1-4):");
    scanf("%d,%d,%d", &M.n, &M.e, &t);
    printf("请输入顶点值,输入格式:number(or char)<回车>:\n");
    for (int a = 1; a <= M.n; a++)
    {
        scanf("%d", &M.vexlist[a].vertex);
        M.vexlist[a].firstedge = NULL; //**
    }
    printf("顶点为:");

    for (int a = 1; a <= M.n; a++)
    {
        printf("V%d\t", M.vexlist[a].vertex);
    }

    //不带权图
    if (t == 1 || t == 2)
    {
        int tail, head, weight;
        printf("\n输入边,输入格式:i,j<回车>，其中,i,j为顶点的存储下标:1<=i<=%d,1<=j<=%d\n", M.n, M.n);
        if (t == 1) //带权无向图
            for (int i = 0; i < M.e; i++)
            {
                scanf("%d,%d", &tail, &head);
                EdgeNode *p = (EdgeNode *)malloc(sizeof(EdgeNode));
                p->adjvex = head;
                //头插法
                p->next = M.vexlist[tail].firstedge;
                M.vexlist[tail].firstedge = p;

                //对称
                p = (EdgeNode *)malloc(sizeof(EdgeNode));
                p->adjvex = tail;
                //头插法
                p->next = M.vexlist[head].firstedge;
                M.vexlist[head].firstedge = p;
            }
        else
        {
            scanf("%d,%d", &tail, &head);
            EdgeNode *p = (EdgeNode *)malloc(sizeof(EdgeNode));
            p->adjvex = head;
            //头插法
            p->next = M.vexlist[tail - 1].firstedge;
            M.vexlist[tail - 1].firstedge = p;
        }

        for (int i = 1; i <= M.n; i++)
        {
            EdgeNode *p;
            p = M.vexlist[i].firstedge;
            printf("V[%d]:   ", i);
            while (p != NULL)
            {
                if (p->next == NULL)
                {
                    printf("%d", p->adjvex);
                }
                else
                {
                    printf("%d->", p->adjvex);
                }
                p = p->next;
            }
            printf("\n");
        }
    }
    //带权图
    if (t == 3 || t == 4)
    {
        int tail, head, weight;
        printf("\n输入边,输入格式,权值:i,j,n<回车>，其中,i,j为顶点的存储下标:1<=i<=%d,1<=j<=%d\n", M.n, M.n);
        if (t == 3) //带权无向图
            for (int i = 0; i < M.e; i++)
            {
                scanf("%d,%d,%d", &tail, &head, &weight);
                EdgeNode *p = (EdgeNode *)malloc(sizeof(EdgeNode));
                p->adjvex = head;
                p->cost = weight;
                //头插法
                p->next = M.vexlist[tail].firstedge;
                M.vexlist[tail].firstedge = p;

                //对称
                p = (EdgeNode *)malloc(sizeof(EdgeNode));
                p->adjvex = tail;
                p->cost = weight;
                //头插法
                p->next = M.vexlist[head].firstedge;
                M.vexlist[head].firstedge = p;
            }
        else
        {
            scanf("%d,%d,%d", &tail, &head, &weight);
            EdgeNode *p = (EdgeNode *)malloc(sizeof(EdgeNode));
            p->adjvex = head;
            p->cost = weight;
            //头插法
            p->next = M.vexlist[tail].firstedge;
            M.vexlist[tail].firstedge = p;
        }

        for (int i = 1; i <= M.n; i++)
        {
            EdgeNode *p;
            p = M.vexlist[i].firstedge;
            printf("V[%d]:   ", i);
            while (p != NULL)
            {
                if (p->next == NULL)
                {
                    printf("%d,%d", p->adjvex, p->cost);
                }
                else
                {
                    printf("%d,%d->", p->adjvex, p->cost);
                }
                p = p->next;
            }
            printf("\n");
        }
    }
    return M;
}
// DFS
void DFS(AdjGraph *G, int i)
//以vi为出发点时对邻接表表示的图G进行先深搜索
{
    int count;
    EdgeNode *p;
    printf("V[%d]", G->vexlist[i].vertex);
    visited[i] = 1;
    //链表的遍历
    p = G->vexlist[i].firstedge;
    while (p)
    {
        if (!visited[p->adjvex])
            DFS(G, p->adjvex);
        p = p->next;
    }
}
void DFSTraverse(AdjGraph G, int a) //主算法
// 先深搜索----邻接表表示的图G；而以邻接矩阵表示G时，算法完全相同
{
    int i;
    int c = 0;
    for (int i = 1; i <= G.n; i++)
        visited[i] = 0; //标志数组初始化
    DFS(&G, a);
    for (i = 1; i <= G.n; i++)
    {
        if (!visited[i])
            DFS(&G, i);
    }
}

// BFS
void BFS(AdjGraph *G, int k)
{ //这里没有进行先广编号
    int i;
    EdgeNode *p;
    Queue Q;
    // MakeNull(Q);
    Q.front = malloc(sizeof(celltype));
    Q.front->next = NULL;
    Q.rear = Q.front;

    printf("V[%d]", G->vexlist[k].vertex);
    visited[k] = 1;
    EnQueue(k, &Q); //进队列
    while (!Empty(Q))
    {                    //队空搜索结束
        i = DeQueue(&Q); // vi出队
        //链表的遍历
        p = G->vexlist[i].firstedge; //取vi的边表头指针
        while (p)
        { //若vi的邻接点 vj(j= p→adjvex)存在,依次搜索
            if (!visited[p->adjvex])
            {                                                  //若vj未访问过
                printf("V[%d]", G->vexlist[p->adjvex].vertex); //访问vj
                visited[p->adjvex] = 1;                        //给vj作访问过标记
                EnQueue(p->adjvex, &Q);                        //访问过的vj入队
            }
            p = p->next; //找vi的下一个邻接点
        }
        // 重复检测 vi的所有邻接顶点
    } //外层循环，判队列空否
} //以vk为出发点时对用邻接表表示的图G进行先广搜索

void BFSTraverse(AdjGraph G, int a) //主算法
//* 先广搜索一邻接表表示的图G；而以邻接矩阵表示G时，算法完全相同
{
    int i, count = 1, c = 0;
    for (int i = 1; i <= G.n; i++)
        visited[i] = 0; //标志数组初始化
    BFS(&G, a);
    for (i = 1; i <= G.n; i++)
    {
        if (!visited[i])
            BFS(&G, i);
    }
}

int main(void)
{
    AdjGraph M;
    int a, b, c;
    //创建邻接表
    M = Create(M);
    //深度优先遍历
    printf("深度优先遍历:\n请输入初始遍历的顶点下标:\n");
    scanf("%d", &a);
    DFSTraverse(M, a);
    printf("\n");
    printf("广度优先遍历:\n请输入初始遍历的顶点下标:\n");
    scanf("%d", &c);
    BFSTraverse(M, c);
}