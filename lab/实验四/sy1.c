#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>

#define NumVertices 100
#define INFINITE 9999
int visited[NumVertices]; //访问标记数组是全局变量,0为false,1为true
int dfn[NumVertices];     //顶点的先深编号
typedef int VerTexData;
typedef int EdgeData;
typedef struct
{
    VerTexData verlist[NumVertices];         //顶点表
    EdgeData edge[NumVertices][NumVertices]; //邻接矩阵的边表
    int n, e;                                //图的顶点数和边数
} MTGraph;

typedef struct celltype
{
    int element;
    struct celltype *next;

} celltype;
typedef struct
{
    celltype *front;
    celltype *rear;
} Queue;

int Empty(Queue Q)
{
    if (Q.front == Q.rear)
        return 1;
    else
        return 0;
}

//入
void EnQueue(int x, Queue *Q)
{
    Q->rear->element = x;
    Q->rear->next = malloc(sizeof(celltype));
    Q->rear = Q->rear->next;
    Q->rear->next = NULL;
}

//出
int DeQueue(Queue *Q)
{
    int e;
    celltype *temp;
    if (Empty(*Q))
        return 0;
    else
    {
        e = Q->front->element;
        temp = Q->front->next;
        Q->front->next = temp->next;
        free(temp);
        if (Q->front->next == NULL)
            Q->rear = Q->front;
    }
    return e;
}

//创建邻接矩阵
MTGraph Create(MTGraph M)
{
    // t:1-无向图； t:2-有向图;  t：3-带权无向图 ;t：4-带权有向图
    int t;
    int vdata;
    int i, j;
    int w; //权值
    printf("请输入顶点数、边数、图的类型，输入格式(均为数字)：v,e,t(t为1-4):");
    scanf("%d,%d,%d", &M.n, &M.e, &t);
    printf("请输入顶点值,输入格式:number(or char)<回车>:\n");
    for (int a = 1; a <= M.n; a++)
    {
        scanf("%d", &vdata);
        M.verlist[a] = vdata;
    }
    printf("顶点为:");
    for (int a = 1; a <= M.n; a++)
    {
        printf("V%d\t", M.verlist[a]);
    }

    //不带权图
    if (t == 1 || t == 2)
    {
        printf("\n输入边,输入格式:i,j<回车>，其中,i,j为顶点的存储下标:1<=i<=%d,1<=j<=%d\n", M.n, M.n);
        for (i = 1; i <= M.n; i++)
        {
            for (j = 1; j <= M.n; j++)
            {
                M.edge[i][j] = 0;
            }
        }
        if (t == 1)
        {
            for (int a = 1; a <= M.e; a++)
            {
                scanf("%d,%d", &i, &j);
                M.edge[i][j] = 1;
                M.edge[j][i] = 1;
            }
        }
        else
        {
            for (int a = 1; a <= M.e; a++)
            {
                scanf("%d,%d", &i, &j);
                M.edge[i][j] = 1;
            }
        }
    }
    //带权图
    if (t == 3 || t == 4)
    {
        printf("\n输入边及其权值,输入格式:(w<9999,1<=i<=%d,1<=j<=%d,):i,j,w\n", M.n, M.n);
        for (i = 1; i <= M.n; i++)
        {
            for (j = 1; j <= M.n; j++)
            {
                M.edge[i][j] = INFINITE;
            }
        }
        if (t == 3)
            for (int a = 1; a <= M.e; a++)
            {
                scanf("%d,%d,%d", &i, &j, &w);
                M.edge[i][j] = w;
                M.edge[j][i] = w;
            }
        else
            for (int a = 1; a <= M.e; a++)
            {
                scanf("%d,%d,%d", &i, &j, &w);
                M.edge[i][j] = w;
            }
    }
    printf("图的矩阵为:\n");
    //输出矩阵
    for (i = 1; i <= M.n; i++)
    {
        for (j = 1; j <= M.n; j++)
        {
            if (M.edge[i][j] == INFINITE)
            {
                printf("∞\t");
            }
            else
            {
                printf("%d\t", M.edge[i][j]);
            }
        }
        printf("\n");
    }
    return M;
}
// DFS
void DFS(MTGraph *G, int i)
//以vi为出发点对邻接矩阵表示的图G进行深度优先搜索
{
    int j, count = 1;
    printf("V%d\t", G->verlist[i]);
    visited[i] = 1; //标记vi已访问
    dfn[i] = count; //对vi进行编号
    count++;        //下一个顶点的编号
    for (j = 0; j < G->n; j++)
        if ((G->edge[i][j] != 0) && (G->edge[i][j] != INFINITE) && !visited[j]) //若vj尚未访问
            DFS(G, j);
}
void DFSTraverse(MTGraph G, int a) //主算法
// 先深搜索----邻接表表示的图G；而以邻接矩阵表示G时，算法完全相同
{
    int i;
    int c = 0;
    for (int i = 1; i <= G.n; i++)
        visited[i] = 0; //标志数组初始化
    DFS(&G, a);
    for (i = 1; i <= G.n; i++)
    {
        if (!visited[i])
            DFS(&G, i);
    }
}
// BFS
void BFS(MTGraph *G, int k)
{
    int i, j;
    Queue Q;
    // MakeNull(Q);
    Q.front = malloc(sizeof(celltype));
    Q.front->next = NULL;
    Q.rear = Q.front;

    printf("V%d\t", G->verlist[k]); //访问vk
    visited[k] = 1;                 //给vk作访问过标记
    EnQueue(k, &Q);                 // vk进队列
    while (!Empty(Q))
    {                    //队空时搜索结束
        i = DeQueue(&Q); // vi出队
        for (j = 1; j <= G->n; j++)
        { //依次搜索vi的邻接点 vj
            if (G->edge[i][j] == 1 && !visited[j])
            {                                   //若vj未访问过
                printf("V%d\t", G->verlist[j]); //访问vj
                visited[j] = 1;                 //给vj作访问过标记
                EnQueue(j, &Q);                 //访问过的vj入队
            }
        } //重复检测 vi的所有邻接顶点
    }     //外层循环，判队列空否
} // 以vk为出发点时对用邻接矩阵表示的图G进行先广搜索

void BFSTraverse(MTGraph G, int a) //主算法
//* 先广搜索一邻接表表示的图G；而以邻接矩阵表示G时，算法完全相同
{
    int i, count = 1, c = 0;
    for (int i = 1; i <= G.n; i++)
        visited[i] = 0; //标志数组初始化
    BFS(&G, a);
    for (i = 1; i <= G.n; i++)
    {
        if (!visited[i])
            BFS(&G, i);
    }
}

int main(void)
{
    MTGraph M;
    int a, b, c;
    //创建邻接矩阵
    M = Create(M);
    //深度优先遍历
    printf("深度优先遍历:\n请输入遍历的初始顶点:\n");
    scanf("%d", &a);
    for (int i = 1; i <= M.n; i++)
    {
        if (a == M.verlist[i])
            b = i;
    }
    printf("遍历序列为:\n");
    DFSTraverse(M, b);
    printf("\n");
    printf("广度优先遍历:\n请输入遍历的初始顶点:\n");
    scanf("%d", &c);
    for (int i = 1; i <= M.n; i++)
    {
        if (c == M.verlist[i])
            b = i;
    }
    printf("遍历序列为:\n");
    BFSTraverse(M, b);
}