#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>

typedef struct BiTnode
{
    char data;
    struct BiTnode *lchild;
    struct BiTnode *rchild;
} BiTnode, *BiTree;

//初始化二叉树
void InitBiTree(BiTree *T)
{
    *T = NULL;
}
//先序创建二叉树
void CreateBiTree(BiTree *T)
{
    char ch;
    scanf("%c", &ch);
    if (ch == '#')
    {
        *T = NULL;
    }
    else
    {
        *T = (BiTree)malloc(sizeof(BiTnode));
        if (!T)
        {
            printf("结点创建失败!\n");
            exit(-1);
        }
        (*T)->data = ch;

        CreateBiTree(&(*T)->lchild);
        CreateBiTree(&(*T)->rchild);
    }
}

//先序循环遍历
void PreOrderTraverse(BiTree T)
{
    if (T != NULL)
    {
        printf("%c\t", T->data);
        PreOrderTraverse(T->lchild);
        PreOrderTraverse(T->rchild);
    }
}

//中序循环遍历
void InOrderTraverse(BiTree T)
{
    if (T != NULL)
    {
        InOrderTraverse(T->lchild);
        printf("%c\t", T->data);
        InOrderTraverse(T->rchild);
    }
}
//后序循环遍历
void PostOrderTraverse(BiTree T)
{
    if (T != NULL)
    {
        PostOrderTraverse(T->lchild);
        PostOrderTraverse(T->rchild);
        printf("%c\t", T->data);
    }
}
int main(void)
{
    BiTree T;
    printf("请输入二叉树的先序遍历序列,用#代表构造空树:\n");
    CreateBiTree(&T);
    printf("先序遍历序列::\n");
    PreOrderTraverse(T);
    printf("\n");
    printf("中序遍历序列:\n");
    InOrderTraverse(T);
    printf("\n");
    printf("后序遍历序列:\n");
    PostOrderTraverse(T);
    printf("\n");
}