#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct student
{
    char xh[12];
    char xm[20];
    float china;
    float math;
    float english;
    float computer;
} stu[40];

//总分求和
float Sum(int i)
{
    return stu[i].china + stu[i].math + stu[i].english + stu[i].computer;
}
//总分降序排序
void BubbleSort(int n)
{
    int flag = 1;
    for (int i = 1; i <= n - 1 && flag == 1; i++)
    {
        flag = 0;
        for (int j = n; j >= i + 1; j--)
        {
            if (Sum(j) > Sum(j - 1))
            {
                flag = 1;
                stu[0] = stu[j];
                stu[j] = stu[j - 1];
                stu[j - 1] = stu[0];
            }
        }
    }
    printf("总成绩排名：学号如下\n");
    for (int i = 1; i <= n; i++)
    {
        printf("%s\n", stu[i].xh);
    }
}
//学号查找
void xh_serach(int n)
{
    printf("请输入要查找的学号：");
    scanf("%s", &stu[0].xh);
    int i;
    for (i = n; strcmp(stu[0].xh, stu[i].xh) != 0; --i)
        ;
    if (i == 0)
        printf("无此学生\n");
    else
        printf("查找成功，下标(即排名)为%d\n", i);
}
//姓名查找
void xm_serach(int n)
{
    printf("请输入要查找的姓名：");
    scanf("%s", &stu[0].xm);
    int i;
    for (i = n; strcmp(stu[0].xm, stu[i].xm) != 0; --i)
        ;
    if (i == 0)
        printf("无此学生\n");
    else
        printf("查找成功，下标(即排名)为%d", i);
}
int main(void)
{
    int n;
    printf("请输入学生个数:");
    scanf("%d", &n);
    printf("\n请依次输入各学生信息,学号、姓名、语文、数学、英语、计算机:\n");
    for (int i = 1; i <= n; i++)
    {
        printf("学生%d--学号、姓名、语文、数学、英语、计算机:\n", i);
        scanf("%s", &stu[i].xh);
        scanf("%s", &stu[i].xm);
        scanf("%f,%f,%f,%f", &stu[i].china, &stu[i].math, &stu[i].english, &stu[i].computer);
        printf("\n");
    }
    //排序
    BubbleSort(n);
    //学号查找
    xh_serach(n);
    //姓名查找
    xm_serach(n);
}