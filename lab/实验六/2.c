#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct student
{
   char xh[12];
   char xm[20];
   float china;
   float math;
   float english;
   float computer;
   struct student *next;
} Node, *List;
// typedef struct
// {
//    Node ver[100];
//    int n;
// } List;

//总分求和
float Sum(List L)
{
   return L->china + L->math + L->english + L->computer;
}
//参数为头结点和需要交换的两个结点的位置（起点为1）
List swap_node(List head, int i)
{
   //第一个结点前一个结点
   Node *a = head;
   for (int x = 0; x < i - 1; x++)
      a = a->next;
   //保存第一个结点
   Node *b = a->next;
   //保存第二结点
   Node *c = b->next;
   //改变pre下一个结点的值
   a->next = c;
   //必须先把b的下一个结点值给a先
   b->next = c->next;
   //让b的下一个结点等于a
   c->next = b;
   return head;
}
//总分降序排序
List BubbleSort(List head, int n)
{
   //用于变量链表
   Node *L = head;
   // //作为一个临时量
   Node *p1;
   //如果链表为空直接返回
   if (n == 0)
      return NULL;
   int flag = 1;

   for (int i = 1; i <= n - 1 && flag == 1; i++)
   {
      L = head->next;
      flag = 0;
      for (int j = 1; j <= n - i; j++)
      {
         if (Sum(L) < Sum(L->next))
         {
            flag = 1;
            head = swap_node(head, j);
            L = head;
         }
         if (flag == 1)
         {
            for (int x = 0; x < j + 1; x++)
            {
               L = L->next;
            }
         }
         else
            L = L->next;
      }
   }
   printf("总成绩排名：学号如下\n");
   List b;
   b = head->next;
   for (int i = 1; i <= n; i++)
   {
      printf("%s\n", b->xh);
      b = b->next;
   }
   return head;
}

//学号查找
void xh_serach(List head, int n)
{
   List L = (List)malloc(sizeof(Node));
   Node p;
   L = head->next;
   printf("请输入要查找的学号：");
   scanf("%s", &p.xh);
   int i;
   for (i = 1; strcmp(p.xh, L->xh) != 0; i++)
   {
      if (i == n + 1)
         break;
      if (i != 0)
         L = L->next;
   }
   if (i == n + 1)
      printf("无此学生\n");
   else
      printf("查找成功，下标(即排名)为%d\n", i);
}
//姓名查找
void xm_serach(List head, int n)
{
   List L = (List)malloc(sizeof(Node));
   Node p;
   L = head->next;
   printf("请输入要查找的姓名：");
   scanf("%s", &p.xh);
   int i;
   for (i = 1; strcmp(p.xh, L->xm) != 0; ++i)
   {
      if (i == n + 1)
         break;
      if (i != 0)
         L = L->next;
   }
   if (i == n + 1)
      printf("无此学生\n");
   else
      printf("查找成功，下标(即排名)为%d", i);
}
int main(void)
{
   List p = (List)malloc(sizeof(Node));
   List q;
   int n;
   p->next = NULL;
   printf("请输入学生个数:");
   scanf("%d", &n);
   printf("\n请依次输入各学生信息,学号、姓名、语文、数学、英语、计算机:\n");
   for (int i = 1; i <= n; i++)
   {
      q = (List)malloc(sizeof(Node));
      printf("学生%d--学号、姓名、语文、数学、英语、计算机:\n", i);
      scanf("%s", &q->xh);
      scanf("%s", &q->xm);
      scanf("%f,%f,%f,%f", &q->china, &q->math, &q->english, &q->computer);
      q->next = p->next;
      p->next = q;
      printf("\n");
   }
   //排序
   p = BubbleSort(p, n);
   //学号查找
   xh_serach(p, n);
   //姓名查找
   xm_serach(p, n);
}
