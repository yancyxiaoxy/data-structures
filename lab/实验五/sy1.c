#include <stdio.h>
#include <malloc.h>

typedef int KeyType;

typedef struct
{
    KeyType key;
} ElemType;

typedef struct
{
    ElemType *R;
    int length;
} SSTable;

//初始化
int initlist_sq(SSTable *st, int len)
{
    st->R =(ElemType*) malloc(sizeof(ElemType) * (len + 1));
    if (!st->R)
        return 0;
    st->length = len + 1;
    return 1;
}
//顺序查找法
int Search_Seq(SSTable St, KeyType key)
{
    int i;
    St.R[0].key = key;
    for (i = St.length; St.R[i].key != key; --i)
        ;
    return i;
}

int create(SSTable *St)
{
    int ky;
    printf("输入元素值:\n");
    St->R[0].key = 0;
    for (int i = 1; i < St->length; i++)
    {
        scanf("%d", &ky);
        St->R[i].key = ky;
    }
    return 1;
}

int main(void)
{
    SSTable St;
    int x, len, a;
    printf("创建顺序表:\n------------------\n");
    printf("请输入创建顺序表的长度:");
    scanf("%d", &len);
    initlist_sq(&St, len) == 1 ? printf("初始化成功\n") : printf("初始化失败\n");
    a = create(&St);
    if (a = 1)
    {
        printf("创建成功\n");
        printf("顺序表为:\n");
        for (int i = 1; i < St.length; i++)
        {
            printf("%d", St.R[i].key);
        }
    }
    printf("\n输入要查找的值x:");
    scanf("%d", &x);
    printf("x在顺序表中的下标为:");
    printf("%d", Search_Seq(St, x));
}