#include <stdio.h>
#include <malloc.h>
#define NULLKEY 0
typedef struct
{
    int key;
} ElemType;

typedef struct
{
    ElemType *R;
    int length;
    int num;
} Hash;

//初始化
int initlist_sq(Hash *st)
{

    printf("请输入哈希表的表长length和元素个数num\n");
    printf("表长length=");
    scanf("%d", &st->length);
    printf("元素个数=");
    scanf("%d", &st->num);
    st->R = (ElemType *)malloc(sizeof(ElemType) * st->length);
    if (st->length < st->num)
    {
        printf("哈希表的关键字数大于表长,哈希表初始化失败!\n");
        return 0;
    }
    else
    {
        for (int i = 0; i < st->length; i++)
        {
            st->R[i].key = NULLKEY;
        }
        return 1;
    }
}

//哈希查找
int HashSearch(Hash *st, int k)
{
    int hash, rehash;
    hash = k % 13;
    if (st->R[hash].key == NULLKEY)
    {
        return 0;
    }
    else if (st->R[hash].key == k)
    {
        return 1;
    }
    else
    {
        for (int i = 1; i < st->length; i++)
        {
            rehash = (rehash + i) % st->length;
            if (st->R[rehash].key == NULLKEY)
            {
                return 0;
            }
            else if (st->R[rehash].key == k)
            {
                return 1;
            }
        }
    }
    printf("表已满");
    return -1;
}

//构造哈希表
int create(Hash *St, int num)
{
    int hash, k;
    int h[St->length];
    printf("请输入哈希表中的%d个关键字:", num);
    for (int i = 0; i < St->num; i++)
        scanf("%d", &h[i]);
    for (int i = 0; i < St->num; i++)
    {
        k = h[i];
        int a = HashSearch(St, k);
        if (a == 1) //有冲突
        {
            printf("输入序列中关键字%d,元素个数num减1\n", k);
            --St->num;
        }
        if (a == 0) //无冲突
        {
            hash = h[i] % 13;
            for (int i = 0; i < St->length; i++)
            {
                hash = (hash + i) % St->length;
                if (St->R[hash].key == NULLKEY)
                {
                    St->R[hash].key = k;
                    break;
                }
            }
        }
    }
}

//输出表中元素
void output(Hash st)
{
    for (int i = 0; i < st.length; i++)
    {
        printf("%d  ", st.R[i].key);
    }
    printf("\n");
}

int main(void)
{
    Hash St;
    int x, a;
    initlist_sq(&St) == 1 ? printf("哈希表初始化成功\n") : printf("哈希表初始化失败\n");
    a = create(&St, St.num);
    if (a = 1)
    {
        printf("创建成功\n");
        printf("输出哈希表的关键字:\n");
        output(St);
    }
}